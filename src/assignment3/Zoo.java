package assignment3;

public class Zoo {
	private example1.BarnYard _by1;
	private example1.BarnYard _by2;
	private example1.BarnYard _by3;
	
	
	
	public Zoo () {
		_by1 = new example1.BarnYard();
		_by2 = new example1.BarnYard();
		_by3 = new example1.BarnYard();
			
	}

	
	public void addPigtoBarnyard() {
		
		example1.Pig pig1;
		pig1 = new example1.Pig();
		_by1.addPig(pig1);
		pig1.start();
		getBarnYard();
	}
	
	
	public void getBarnYard() {
			
		example1.BarnYard temp;
	//	temp = new example1.BarnYard();
        temp = _by1;
		_by1 = _by2;
		_by2 = _by3;
		_by3 = temp;
					
	}
	
}
