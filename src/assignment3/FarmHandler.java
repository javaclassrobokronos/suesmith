package assignment3;

public class FarmHandler {
	
	private Farm _farm;
	
	public FarmHandler () {
		// examples of composition below?
		_farm = new Farm();
		_farm.addMovingChicken();
	}

}
