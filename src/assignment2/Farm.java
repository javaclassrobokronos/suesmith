package assignment2;

public class Farm {
	
	private example1.BarnYard _by;
	private example1.Chicken _ck;
	private example1.Butterfly _bf;
	private example1.Pig _pg;
	
		public Farm () {
		_by = new example1.BarnYard();	
		createAnimals();
		startAnimals();
		
	}
   
    public void createAnimals() {
    	
    	_ck = new example1.Chicken();
    	_by.addChicken(_ck);
    	_bf = new example1.Butterfly();
    	_by.addButterfly(_bf);
    	_pg = new example1.Pig();
    	_by.addPig(_pg);
    	    	   	  	
    }
	public void startAnimals () {
		_ck.start();
		_bf.start();
		_pg.start();		
	}
}
