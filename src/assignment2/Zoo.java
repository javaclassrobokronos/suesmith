package assignment2;

  public class Zoo {
	   
	   private example1.BarnYard _by1;
	   private example1.BarnYard _by2;
	   private example1.BarnYard _by3;
	   private example1.Chicken  _ck;
	   private example1.Pig _pig;
	   private example1.Butterfly _bf;
	
	
	public Zoo () {
		
		_by1 = new example1.BarnYard();
		_by2 = new example1.BarnYard();
		_by3 = new example1.BarnYard();
		 addBarnYard1();
		 addBarnYard2();
		 addBarnYard3();
		
	}
     public void createMovingChicken(){ 
      _ck = new example1.Chicken();
      _ck.start();
     }
     
     public void createMovingPig(){ 
   	  _pig = new example1.Pig();
      _pig.start();
    }
     
     public void createMovingButterfly(){ 
      _bf = new example1.Butterfly();
      _bf.start();
       }
     
     
     public void addBarnYard1(){
    	 createMovingChicken();
    	 _by1.addChicken(_ck);
    	 createMovingPig();
    	 _by1.addPig(_pig);  	  	
     }
     
     public void addBarnYard2(){
    	 createMovingButterfly();
    	 _by2.addButterfly(_bf);
    	 createMovingPig();
    	 _by2.addPig(_pig);    	 
     }
     
     public void addBarnYard3(){
    	 createMovingButterfly();
    	 _by3.addButterfly(_bf);
    	 createMovingChicken();
    	 _by3.addChicken(_ck);
    	 _bf.stop();   	 
     }
       
  }