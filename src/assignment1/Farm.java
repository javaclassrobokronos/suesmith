package assignment1;

public class Farm {
	public Farm () {
		example1.BarnYard by;
		by = new example1.BarnYard ();
		example1.Chicken ck1;
		ck1 = new example1.Chicken ();
		example1.Chicken ck2;
		ck2 = new example1.Chicken ();
		example1.Chicken ck3;
		ck3 = new example1.Chicken ();
		by.addChicken(ck1);
		by.addChicken(ck2);
		by.addChicken(ck3);
		ck1.start();
		ck2.start();
		example1.Pig pg1;
		pg1 = new example1.Pig ();
		example1.Pig pg2;
		pg2 = new example1.Pig ();
		by.addPig(pg1);
		by.addPig(pg2);
		pg1.start();
		pg2.start();
		example1.Butterfly btf;
		btf = new example1.Butterfly();	
		by.addButterfly(btf);
	}
}
