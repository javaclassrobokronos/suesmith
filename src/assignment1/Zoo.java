package assignment1;

public class Zoo {
	public Zoo () {
		
		example1.BarnYard by1;
		by1 = new example1.BarnYard ();
		example1.Chicken ck1;
		ck1 = new example1.Chicken ();
		example1.Chicken ck2;
		ck2 = new example1.Chicken ();
		by1.addChicken(ck1);
		by1.addChicken(ck2);
		ck1.start();
	
		example1.BarnYard by2;
		by2 = new example1.BarnYard ();
		example1.Butterfly btf1;
		btf1 = new example1.Butterfly();	
		example1.Butterfly btf2;
		btf2 = new example1.Butterfly();
		example1.Butterfly btf3;
		btf3 = new example1.Butterfly();
		by2.addButterfly(btf1);
		by2.addButterfly(btf2);
		by2.addButterfly(btf3);
		btf1.start();
		btf2.start();
		btf3.start();
		
		example1.BarnYard by3;
		by3 = new example1.BarnYard ();
		example1.Pig pg1;
		pg1 = new example1.Pig ();
		example1.Pig pg2;
		pg2 = new example1.Pig ();
		by3.addPig(pg1);
		by3.addPig(pg2);
		pg1.start();		
	}
}
