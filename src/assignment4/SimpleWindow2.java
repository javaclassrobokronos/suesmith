package assignment4;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class SimpleWindow2 {
	
	private JFrame _frame;
	private JButton _button;
	private JLabel _label;
	
 
	public SimpleWindow2() {
		_frame = new JFrame ("Window with Label");
		_frame.setVisible(true);
		_frame.setLayout(new GridLayout(5,20));
		
		_button = new JButton("Click Here");
		_frame.add(_button);
		_button.addActionListener(new SimpleWindow2Handler());
	  
		_label = new JLabel ();
		_frame.add(_label);		
		_label.setText("I am a label");
		
		_frame.pack();
		
	}

}
