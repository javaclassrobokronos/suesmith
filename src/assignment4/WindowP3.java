package assignment4;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

//Problem 3 window
public class WindowP3 {
	
	private JFrame _p3frame;
	private JButton _leftbutton;
	private JButton _rightbutton;
	private JLabel _leftbuttonlabel;
	private JLabel _rightbuttonlabel;
	
	public WindowP3(){
		_p3frame = new JFrame ("Problem3");
		_p3frame.setVisible(true);
		_p3frame.setLayout(new GridLayout(2,2));
			
		_leftbutton = new JButton ("left");
		_rightbutton = new JButton ("right");
		
		_leftbuttonlabel = new JLabel ();
		_rightbuttonlabel = new JLabel ();
			
		_p3frame.add(_leftbutton);
		_p3frame.add(_rightbutton);
		_p3frame.add(_leftbuttonlabel);
		_p3frame.add(_rightbuttonlabel);
				
		_leftbutton.addActionListener(new LeftButtonHandler(this));
	    _rightbutton.addActionListener(new RightButtonHandler(this));
		
		_p3frame.pack();
	//	_p3frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	 public void setLeftLabelText (String m)  {
		  
		 _leftbuttonlabel.setText(m);
	
		 
	 }

	 public void setRightLabelText (String m)  {
		  
		 _rightbuttonlabel.setText(m);
		 
		 
	 }
	 
	 
}
