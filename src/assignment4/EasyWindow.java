package assignment4;

import javax.swing.JButton;
import javax.swing.JFrame;

public class EasyWindow {
	
	private JFrame _frame;
	private JButton _button;
	
	
	public EasyWindow () {
		_frame = new JFrame ("SueSmithWindow");
		_frame.setVisible(true);
		
		_button = new JButton("Press");
		_frame.add(_button);
		_button.addActionListener(new EasyWindowHandler());
		_frame.pack();
		
	}

}
