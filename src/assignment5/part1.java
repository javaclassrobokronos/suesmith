package assignment5;

public class part1 {
	
	
	    private boolean _bval1 = true;
	    private boolean _bval0 = false;
		private int _ival1;
		private int _ival2;
		private int _ival3;
		private String _sval1;
		private String _sval2;
		private String _sval12;
	
	public part1() {
			
		 
		// 5 < 3  is false
		 _ival1 = 5;
		 _ival2 = 3;
		 if (_ival1 <  _ival2 ) {
			 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
		 }
		//  7 == 4 && true  is false
		 _ival1 = 7;
		 _ival2 = 4;
		 if ((_ival1  ==  _ival2)  && _bval1)  {
			 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
			 }
		 // true != false  is true
		 if (_bval1 != _bval0) {
			 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
			 }
	     // true || (true && false) is true
		  if (_bval1 || (_bval1 && _bval0)) {
			  System.out.println("true");
			 }
				 else {
					 System.out.println("false");
			 }
		  
		 // (true || true) && false is false
	     if ((_bval1 ||_bval1) && _bval0) {
		  System.out.println("true");
		 }
			 else {
				 System.out.println("false");
		 }
	     //"yes" == "ye" + "s" is false (because you should not use == for strings
	     _sval1= "ye";
	     _sval2 = "s";
	     _sval12 = _sval1 + _sval2;
	     System.out.println(_sval12);
	     // this will print yes
	     if (_sval12 == _sval1 + _sval2) {
	    	 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
	     }
	     // the answer is false
	     //8 == 5 + 3   is true
	     _ival1=5;
	     _ival2=3;
	     _ival3=8;
	     if (_ival3 == _ival1 + _ival2 ) {
	    	 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
	     }
	     //"8" == "5" + 3 is false
	     _sval1="8";
	     _sval2="5";
	     if ( _sval1 == _sval2 + _ival2) {
	    	 System.out.println("true");
		 }
			 else {
				 System.out.println("false");
	     }
	     
	     
	}	
	}

