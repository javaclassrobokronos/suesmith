package assignment5;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;

public class WindowButton3 {
	
	private JFrame _wb3frame;
	private JButton _wb3button1;
	private JButton _wb3button2;
	private JButton _wb3button3;
	
	
	
	public WindowButton3 () {
		_wb3frame = new JFrame ("Assignment 5.3");
		_wb3frame.setVisible(true);
		_wb3frame.setLayout(new GridLayout (1,3));
		
		_wb3button1 = new JButton();
		_wb3button2 = new JButton();
		_wb3button3 = new JButton();
		
		
		_wb3button1.setBackground(Color.GRAY);
		_wb3button2.setBackground(Color.BLUE);
		_wb3button3.setBackground(Color.GRAY);
				
		_wb3frame.add(_wb3button1);
		_wb3frame.add(_wb3button2);
		_wb3frame.add(_wb3button3);
		
		_wb3button1.addActionListener(new B1ButtonHandler(this));
	//	_wb3button2.addActionListener(new B2ButtonHandler(this));
	//  _wb3button3.addActionListener(new B3ButtonHandler(this));	
		
		_wb3frame.pack();	
	}
	
		
	public void setColorBackground1 (Color m)  {
	  _wb3button1.setBackground(m);
	}
}	 