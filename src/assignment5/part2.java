package assignment5;

import java.util.ArrayList;

public class part2 {
	
	int _count;

	public part2() {
		
	//a) this will produce a 2
		 if (5 > 3 && 7 <= 7) {
			 System.out.println(2);
		 }
		 else {
			 System.out.println(3);
		 }
	//b) this will produce nothing since the statement is false
		 if (5 != 5) {
			 System.out.println("true");
		 }
	//c)  i = 0
	//	  i = 1
	//    i = 2
	//    i = 3	 
		 
		 for (int i = 0; i < 4; i = i +1) {
			 System.out.println("i = " + i );
		 }
	
	// d) 1
	//    2
	//    3
    //    4
		   int dcount = 0;
		   for (boolean b = true; b; b = dcount <= 3){
		    dcount = dcount + 1;
		    System.out.println(dcount);
		   }
	
	// e) 1
	//    2
	//    3
    //    4
	//    5	   
		   int ecount = 0;
		   while( 5> ecount){
		   ecount = ecount + 1;
		   System.out.println(ecount);
		   }
	// f) this prints no 
		   ArrayList<String> strings = new ArrayList<String>();
		   strings.add("yes");
		   strings.add("no");
		   strings.add("maybe");
		   System.out.println(strings.get(1));
	  // g) this prints maybe 
		   ArrayList<String> strings2 = new ArrayList<String>();
		   strings2.add("yes");
		   strings2.add("no");
		   strings2.add("maybe");
		   strings2.remove(0);
		   System.out.println(strings2.get(1));

	}
}
